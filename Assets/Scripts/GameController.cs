﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    [SerializeField]
    Vector2Int itemsCount;
    
    [SerializeField]
    Card cardPrefab;

    [SerializeField]
    RectTransform fieldTransform;

    [SerializeField]
    float restartDelay;
    
    readonly List<Card> cards = new List<Card>();
    readonly List<Vector2Int> freeCoords = new List<Vector2Int>();
    
    Card selectedCard;
    int score;

    public static event Action<int> ScoreUpdate = delegate {  }; 

    int Score
    {
        get => score;
        set
        {
            score = value;
            ScoreUpdate.Invoke(score);
        }
    }

    void Start()
    {
        for (int i = 0; i < itemsCount.x; i++)
        {
            for (int j = 0; j < itemsCount.y; j++)
            {
                CreateCard(new Vector2Int(i, j));
            }
        }
        
        List<Texture2D> textures = ContentManager.Instance.Textures;

        for (int textureIndex = 0, cardIndex = 0; textureIndex < textures.Count && cardIndex < cards.Count; textureIndex++)
        {
            Texture2D t = textures[textureIndex];
            Sprite sprite = Sprite.Create(t, new Rect(0, 0, t.width, t.height),
                new Vector2(0.5f, 0.5f));

            SetCard(cards[cardIndex++], sprite);
            SetCard(cards[cardIndex++], sprite);
        }

        Score = 0;
    }

    void CreateCard(Vector2Int coords)
    {
        
        Card card = Instantiate(cardPrefab, fieldTransform);
        card.RectTransform.localPosition = Vector3.zero;
        card.RectTransform.localScale = Vector3.one;
                
        card.OnShow += OnCardSelected;
        cards.Add(card);
        freeCoords.Add(coords);
    }
    
    void SetCard(Card card, Sprite sprite)
    {
        Vector2Int coords = freeCoords[Random.Range(0, freeCoords.Count)];
        freeCoords.Remove(coords);
        
        Vector2 position = GetBrickPosition(coords);
        card.Init(position, sprite);
    }

    void OnCardSelected(Card card)
    {
        StartCoroutine(CheckCards(card));
    }

    IEnumerator CheckCards(Card card)
    {
        if (selectedCard == null)
        {
            selectedCard = card;
            yield break;
        }

        if (selectedCard.Sprite.Equals(card.Sprite))
        {
            Score++;
            
            card.Reset();
            selectedCard.Reset();

            cards.Remove(card);
            cards.Remove(selectedCard);
            
            selectedCard = null;

            if (cards.Count != 0) yield break;
            
            yield return new WaitForSeconds(restartDelay);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

            yield break;
        }

        card.Hide();
        selectedCard.Hide();
        selectedCard = null;
    }

    Vector2 GetBrickPosition(Vector2 coords)
    {
        Vector2 brickSize = new Vector2
        {
            x = fieldTransform.rect.width / (itemsCount.x + 1),
            y = fieldTransform.rect.height / (itemsCount.y + 1)
        };

        Vector2 offset = brickSize * new Vector2((itemsCount.x + 1) / 2f - 1, (itemsCount.y + 1) / 2f - 1);
        Vector2 brickPosition = Vector2.Scale(coords, brickSize) - offset;

        return brickPosition;
    }
}
