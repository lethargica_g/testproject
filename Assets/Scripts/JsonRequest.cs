﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class JsonRequest
{
    public event Action<string> OnComplete = delegate { };

    readonly string url;
    
    public JsonRequest(string url)
    {
        this.url = url;
    }

    public void Load()
    {
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SendWebRequest().completed += RequestOnCompleted;
    }

    void RequestOnCompleted(AsyncOperation operation)
    {
        UnityWebRequest webRequest = ((UnityWebRequestAsyncOperation) operation).webRequest;

        if (webRequest.isHttpError || webRequest.isNetworkError)
        {
            OnComplete?.Invoke(string.Empty);
            return;
        }

        OnComplete?.Invoke(webRequest.downloadHandler.text);
    }
}
