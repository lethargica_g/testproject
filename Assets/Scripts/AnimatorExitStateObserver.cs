﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnimatorExitStateObserver : StateMachineBehaviour
{
    class ObserverData
    {
        public readonly int state;
        public readonly Action callback;

        public ObserverData(int state, Action callback)
        {
            this.state = state;
            this.callback = callback;
        }
    }

    readonly List<ObserverData> exitData = new List<ObserverData>();
    
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        List<ObserverData> data = exitData.Where(d => d.state.GetHashCode() == stateInfo.shortNameHash).ToList();
        data.ForEach(d =>
        {
            exitData.Remove(d);
            d.callback?.Invoke();
        });
    }

    public void ObserveStateExit(int state, Action callback)
    {
        ObserverData data = new ObserverData(state, callback);
        exitData.Add(data);
    }
}
