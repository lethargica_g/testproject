﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreCounter : MonoBehaviour
{
    Text label;

    void Awake()
    {
        label = GetComponent<Text>();
        GameController.ScoreUpdate += UpdateScore;
    }
    
    void OnDestroy()
    {
        GameController.ScoreUpdate -= UpdateScore;
    }

    void UpdateScore(int value)
    {
        label.text = "Score: " + value;
    }
}