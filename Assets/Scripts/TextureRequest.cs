﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class TextureRequest
{
    public event Action<Texture2D> OnComplete = delegate { };

    readonly string url;

    public TextureRequest(string url)
    {
        this.url = url;
    }

    public void Load()
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        request.SendWebRequest().completed += RequestOnCompleted;
    }
    
    void RequestOnCompleted(AsyncOperation operation)
    {
        UnityWebRequest webRequest = ((UnityWebRequestAsyncOperation) operation).webRequest;

        if (webRequest.isHttpError || webRequest.isNetworkError)
        {
            OnComplete?.Invoke(null);
            return;
        }
        
        OnComplete?.Invoke(DownloadHandlerTexture.GetContent(webRequest));
    }
}
