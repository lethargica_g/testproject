using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class ContentManager
{
    public event Action<bool> ContentLoaded = delegate { };

    public readonly List<Texture2D> Textures = new List<Texture2D>();
    readonly Queue<TextureRequest> texturesRequests = new Queue<TextureRequest>();
    GameContent content;

    const string url = "https://drive.google.com/uc?export=download&id=13PO8l7oRyDT347wgQhq0T273hbYcwaYr";

    static ContentManager instance;
    public static ContentManager Instance => instance ?? (instance = new ContentManager());

    public void LoadContent()
    {
        JsonRequest request = new JsonRequest(url);
        request.OnComplete += OnDataLoadComplete;
        request.Load();
    }

    void OnDataLoadComplete(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            ContentLoaded.Invoke(false);
            return;
        }

        content = JsonConvert.DeserializeObject<GameContent>(text);

        LoadCardsData();
    }

    void LoadCardsData()
    {
        foreach (CardData data in content.cards)
        {
            TextureRequest request = new TextureRequest(data.url);
            request.OnComplete += OnCardsDataLoadComplete;
            texturesRequests.Enqueue(request);
        }

        LoadCardData();
    }

    void LoadCardData()
    {
        if (texturesRequests.Count == 0)
        {
            ContentLoaded.Invoke(true);
            return;
        }

        TextureRequest request = texturesRequests.Dequeue();
        request.Load();
    }


    void OnCardsDataLoadComplete(Texture2D texture)
    {
        if (texture == null)
        {
            LoadCardsData();
            return;
        }

        Textures.Add(texture);
        LoadCardData();
    }
}