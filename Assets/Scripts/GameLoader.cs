﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameLoader : MonoBehaviour
{
    [SerializeField]
    Text loadingText;

    [SerializeField]
    Button retryButton;

    [SerializeField]
    Button playButton;
    
    void Start()
    {
        playButton.onClick.AddListener(LoadScene);
        retryButton.onClick.AddListener(LoadContent);
        
        LoadContent();
    }

    void LoadContent()
    {
        loadingText.gameObject.SetActive(true);

        ContentManager.Instance.ContentLoaded += OnLoadResult;
        ContentManager.Instance.LoadContent();
    }

    void LoadScene()
    {
        SceneManager.LoadScene("Game");
    }
    
    void OnLoadResult(bool result)
    {
        ContentManager.Instance.ContentLoaded -= OnLoadResult;

        loadingText.gameObject.SetActive(false);

        playButton.gameObject.SetActive(result);
        retryButton.gameObject.SetActive(!result);
    }
}