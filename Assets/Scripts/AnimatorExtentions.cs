﻿using System;
using UnityEngine;

public static class AnimatorExtentions
{
    public static void ObserveStateExit(this Animator animator, int state, Action callback)
    {
        animator.GetBehaviour<AnimatorExitStateObserver>().ObserveStateExit(state, callback);
    }
}