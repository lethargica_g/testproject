﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card : MonoBehaviour, IPointerClickHandler
{
    public event Action<Card> OnShow = delegate {  };

    [SerializeField]
    Image image;
    [SerializeField]
    Animator animator;

    [SerializeField]
    float startDelay = 5;

    [SerializeField]
    float hideDelay = 1;

    readonly int animationSpawn = Animator.StringToHash("Spawn");
    readonly int animationShow = Animator.StringToHash("Show");
    readonly int animationHide = Animator.StringToHash("Hide");
    readonly int animationDisable = Animator.StringToHash("Disable");

    bool isShown;

    RectTransform rectTransform;
    public RectTransform RectTransform => rectTransform ? rectTransform : rectTransform = GetComponent<RectTransform>();
    public Sprite Sprite => image.sprite;
    
    public void Init(Vector2 position, Sprite sprite)
    {
        RectTransform.anchoredPosition = position;
        image.sprite = sprite;
        
        SpawnCard();
    }

    void SpawnCard()
    {
        isShown = true;
        
        animator.SetTrigger(animationSpawn);
        animator.ObserveStateExit(animationSpawn, OnAnimationSpawnComplete);
    }

    void Show()
    {
        isShown = true;
        
        animator.SetTrigger(animationShow);
        animator.ObserveStateExit(animationShow, OnAnimationShowComplete);
    }

    public void Hide()
    {
        DelayAction(
            hideDelay,
            () =>
            {
                animator.SetTrigger(animationHide);
                animator.ObserveStateExit(animationHide, OnAnimationHideComplete);
            }
        );
    }

    public void Reset()
    {
        DelayAction(hideDelay, Disable);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isShown) return;

        Show();
    }

    void OnAnimationSpawnComplete()
    {
        DelayAction(startDelay, Hide);
    }
    
    void OnAnimationShowComplete()
    {
        OnShow?.Invoke(this);
    }

    void OnAnimationHideComplete()
    {
        isShown = false;
    }

    void Disable()
    {
        animator.SetTrigger(animationDisable);
    }

    void DelayAction(float delay, Action action)
    {
        StartCoroutine(DoDelayAction(delay, action));
    }

    IEnumerator DoDelayAction(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action?.Invoke();
    }
}
